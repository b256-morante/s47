// document-querySelector -> use it to retrieve an element from the webpage
/*
	document.getElementById('txt-first-name');
	document.getElementsByClassName('txt-input');
	document.getElementsByTagName('input')
*/
const textFirstName = document.querySelector('#txt-first-name');
const textLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// action is considered as an event

/*
textFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = textFirstName.value;
});

textLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = textLastName.value;
});
*/

function combineName() {
	spanFullName.innerHTML = textFirstName.value + textLastName.value;
}

textFirstName.addEventListener('keyup', combineName);
textLastName.addEventListener('keyup', combineName);
